// James Lu
// CS2 Week 9/10
// Othello

Milestone 1: Only implementing such that the player works on random moves. Will
build full AI over the weekend. ^^



Milestone 2: Updates to Heuristic and Decision Trees

=== Flips ===
Obviously moves that flip over more tiles are better than ones that don't flip
that many tiles over.

Implement moves struct with the appropriate score tracking member.

## Implemented in tandem with the Land Value heuristic, so no data on the
## helpfulness of this heuristic alone. I would assume it would be very good.


=== Land Value ===
Corner positions are invaluable (cannot be reclaimed) and by extension, spots
adjacent to corners are undesirable (allow corner capture for opponent). Other
spots are less important to score, but generally the closer to an edge the
better.

We implement an array with position weights to multiply our score by.

## This implementation allows us to beat SimplePlayer consistently, and win 50%
## of the time against ConstantTimePlayer


=== Open squares ===
Moves that open up the board to the opponent (high mobility) will cause us
problems in the long run.

Divide scores by an amount related to the # of open moves available to opponent

## My implementation of this mobility factor seems to actually make my AI worse
## Considering how well Land Value has improved heuristics, I removed this
## implementation from our AI.
##
## Reimplemented with the usage of depth decision tree search.


=== Decision Tree ===
By examining moves n moves ahead, we can make an informed decision on what moves
will be better for us in the long run.

This should be implemented as many copies of boards, and scores of each branch.

## Implementation considers players that go down in successively shallower depth
## ie (first considers depth 4, next considers depth 3, etc). Each depth makes
## the best move given their search depth, and then the layer above makes a move
## based on the assumption that the next move will be calculated as in this
## model.
## We use depth 5 because of time constraints.
## Allows consistent defeat of ConstantTimePlayer
##
## Admittedly this is pretty slow because there is no pruning
## Removal of moves in a layer above (judged by their score after 1 itteration)
## would be a good idea...

BetterPlayer is so good >.<
