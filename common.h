#ifndef __COMMON_H__
#define __COMMON_H__

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y, score;
    Move() {
        this->x = 0;
        this->y = 0;
        this->score = 0;
    }
    Move(int x, int y) {
        this->x = x;
        this->y = y;
        this->score = 0;    
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }
    int getscore() {return score; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
    void setscore(int s) { this->score = s; }
    
    bool operator<(const Move& other) const {
        return this->score < other.score;
    }
};

#endif
