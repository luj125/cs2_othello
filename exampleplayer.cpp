#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
     board = Board();
     me = side;
     opponent = (side == BLACK) ? WHITE : BLACK;
     depth = 0;
}

/*
 * Constructor for the player; initialize everything here. For use in decision
 * tree
 */
ExamplePlayer::ExamplePlayer(Side side, int d) {
     board = Board();
     me = side;
     opponent = (side == BLACK) ? WHITE : BLACK;
     depth = d + 1;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}

/*
 * Modifier for the board
 */
void ExamplePlayer::setBoard(Board b) {
    this->board = b;
}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

    // Alter the board according to the opponentsMove
    board.doMove(opponentsMove, opponent);
    
    // Make sure we have moves to make
    if (!board.hasMoves(me)) return NULL; // Pass if we don't
    
    // Get all the valid moves
    Move *move = new Move();
    list<Move> validMoves = board.valid_moves(me);
    list<Move>::iterator itt;
    
    // Update the move scores by looking ahead
    // Only do so if we are not too deep (time constraint)
    if (depth < 5) {
        for (itt = validMoves.begin(); itt != validMoves.end(); itt++) {
            int currscore = (*itt).getscore();
            Board *oppBoard = board.copy();
            oppBoard->doMove(&(*itt), me);
            // Set up the theoretical player for the next layer
            ExamplePlayer *opp = new ExamplePlayer(opponent, depth);
            opp->setBoard(*(board.copy()));
            // How many moves are available given I make this move?
            int oppmoves = oppBoard->valid_moves(opponent).size();
            // Which move [s]he do given I make this move?
            Move *oppmove = opp->doMove(&(*itt), msLeft);
            if (oppmove == NULL) {
                // We made him skip! THIS IS A GREAT MOVE
                (*itt).setscore(currscore * 100);
                break;
            }
            // What score is this move for him?
            int oppscore = oppBoard->checkMove(oppmove, opponent);
            // Alter our score accordingly
            (*itt).setscore(currscore - (oppscore + 2 * oppmoves));
            delete opp;
        }
    }
    
    validMoves.sort();
    // Take the "best move"
    move->setX(validMoves.back().getX());
    move->setY(validMoves.back().getY());
    board.doMove(move, me);
    return move;
}

