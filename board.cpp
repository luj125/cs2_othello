#include "board.h"

// Land value heuristic
static int landvalue[8][8] = {
{5, 1, 3, 3, 3, 3, 1, 5},
{1, 1, 2, 2, 2, 2, 1, 1},
{4, 2, 3, 3, 3, 3, 2, 4},
{4, 2, 3, 1, 1, 3, 2, 4},
{4, 2, 3, 1, 1, 3, 2, 4},
{4, 2, 3, 3, 3, 3, 2, 4},
{1, 1, 2, 2, 2, 2, 1, 1},
{5, 1, 4, 4, 4, 4, 1, 5}};

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(8 * 3 + 3);
    taken.set(8 * 3 + 4);
    taken.set(8 * 4 + 3);
    taken.set(8 * 4 + 4);
    black.set(8 * 3 + 4);
    black.set(8 * 4 + 3);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

// Check if a square is occupied
bool Board::occupied(int x, int y) {
    return taken[8*y + x];
}

// Check what color is at a square
// First checks if it is occupied
bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[8*y + x] == (side == BLACK));
}

// Makes a change on the board
void Board::set(Side side, int x, int y) {
    taken.set(8*y + x);
    black.set(8*y + x, side == BLACK);
}

// Checks if coordinates are on the board
bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side) != 0) return true;
        }
    }
    return false;
}

/*
 * Returns the number of flipped pieces if a move is legal for the given side;
 * Returns 0 otherwise.
 */
int Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    // We should be able to remove this in the end, unnecessary operation
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();
    int flips = 0;

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return 0;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            int curr = 0;
            while (onBoard(x, y) && get(other, x, y)) {
                x += dx;
                y += dy;
                curr++;
            }
            // Gathered flips, make sure we reach an end
            if (onBoard(x, y) && get(side, x, y)) flips += curr;
        }
    }
    return flips;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    // Again, should be able to remove this
    if (checkMove(m, side) == 0) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Returns a list of a side's valid moves
 */
list<Move> Board::valid_moves(Side side) {
    // Loop through the board
    list<Move> ret;
    for (int x = 0; x < 8; x++) {
        for (int y = 0; y < 8; y++) {
            // Check each square if it is valid
            Move move(x, y);
            move.setscore(landvalue[x][y] * checkMove(&move, side));
            // If the score > 0, then this is valid
            if (move.getscore() != 0) ret.push_back(move);
        }
    }
    return ret;
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}
