#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
    Board board;
    Side me;
    Side opponent;
    int depth;
    
    
public:
    ExamplePlayer(Side side);
    ExamplePlayer(Side side, int d);
    ~ExamplePlayer();
    
    void setBoard(Board b);
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
